# sense8

Weather station on raspberry pi using docker.

- advertising other Bluetooth as Environment sensor
- use telegraf to send data outside
- use redis as data store
- display using luma.core

## Install

- docker engine must be installed

```sh
./scripts/sense8-install
```

## Build images

```sh
export DOCKERHUB_USER=showi
make
```

- see [common.mk](scripts/common.mk) for supported platforms.
