#!/usr/bin/env python3

import sys
import os

module_path = os.path.abspath(os.path.join(os.path.dirname(__file__)))

sys.path.insert(0, module_path)

if __name__ == "__main__":
    import logging

    logging.basicConfig(
        level=logging.DEBUG,
        format="%(asctime)s [%(levelname)s] %(message)s",
        handlers=[
            logging.StreamHandler(),
        ],
    )
    from sowatch.logger import getLogger
    from sowatch.context import make_context
    from sowatch.device import list_devices
    from sowatch.monitor import Monitor
    import time

    logger = getLogger(__file__)
    ctx = make_context()
    for device in list_devices(ctx, DEVTYPE="partition"):
        logger.debug(device)
    monitor = Monitor(ctx=ctx, filters=[], handle=lambda *a: logger.debug(*a))
    monitor.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        logger.debug("bye")
    monitor.stop()
