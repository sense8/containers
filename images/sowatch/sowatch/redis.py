import json
from redis import StrictRedis
from ble.config import base_config
from ble.logger import getLogger

logger = getLogger(__file__)

redis_client = None


class MissingSensorError(Exception):
    pass


def get_client() -> StrictRedis:
    global redis_client
    if redis_client is None:
        redis_client = StrictRedis(**base_config["redis"])
        redis_client.ping()
    return redis_client
