import os

base_path = os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir))

base_config = {
    "redis": {"host": "redis", "port": 6379, "db": 0},
}
