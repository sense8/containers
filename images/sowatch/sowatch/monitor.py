import pyudev
from sowatch.logger import getLogger

logger = getLogger(__file__)


def monitor_log_event(action, device):
    logger.debug("action: {}, device: {}".format(action, device))
    # if "ID_FS_TYPE" in device:
    #     with open("filesystems.log", "a+") as stream:
    #         print("{0} - {1}".format(action, device.get("ID_FS_LABEL")), file=stream)


class Monitor:
    def __init__(
        self,
        ctx: pyudev.Context,
        handle: callable = monitor_log_event,
        filters: [] = ["block"],
    ):
        self.monitor = pyudev.Monitor.from_netlink(ctx)
        # [self.monitor.filter_by(filter_by) for filter_by in filters]
        self.observer = pyudev.MonitorObserver(self.monitor, handle)

    def start(self):
        self.observer.connect("device-event", device_event)
        self.observer.start()

    def stop(self):
        self.observer.disconnect()
        self.observer.stop()
