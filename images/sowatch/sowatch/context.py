import pyudev


def make_context() -> pyudev.Context:
    return pyudev.Context()