import pyudev


def list_devices(ctx: pyudev.Context, subsystem="block", **ka):
    for device in ctx.list_devices(subsystem=subsystem, **ka):
        yield device


def find_device_parent(device, subsystem="block"):
    return device.find_parent(subsystem).device_node
