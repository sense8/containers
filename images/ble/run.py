#!/usr/bin/env python3

import sys
import os

module_path = os.path.abspath(os.path.join(os.path.dirname(__file__)))

sys.path.insert(0, module_path)

if __name__ == "__main__":
    import logging

    logging.basicConfig(
        level=logging.DEBUG,
        format="%(asctime)s [%(levelname)s] %(message)s",
        handlers=[
            logging.StreamHandler(),
        ],
    )
    from ble.utils import make_config
    from ble.config import base_config
    from ble.server import serve

    serve(make_config(base_config))
