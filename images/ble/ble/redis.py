import json
from redis import StrictRedis
from ble.config import base_config
from ble.logger import getLogger

logger = getLogger(__file__)

redis_client = None

class MissingSensorError(Exception):
    pass

def get_client() -> StrictRedis:
    global redis_client
    if redis_client is None:
        redis_client = StrictRedis(**base_config["redis"])
        redis_client.ping()
    return redis_client


def get_sensor_list(client: StrictRedis):
    return client.smembers('Sensors')

def get_sensor_data(client: StrictRedis, sensor_name: str = 'bme680', sensor_id: int = 0):
    fqn = '{}.{}'.format(sensor_name, sensor_id).encode('utf8')
    return json.loads(client.get(fqn))
