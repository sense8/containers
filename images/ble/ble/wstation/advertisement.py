from ble.gatt.advertisement import Advertisement
from ble.wstation.services.climate import ClimateService
import uuid


class WstationAdvertisement(Advertisement):
    PATH_BASE = "/org/bluez/wstation/advertisement"
    def __init__(self, bus, index):
        self.BASE_PATH = "/org/bluez/sense8/advertisement"
        Advertisement.__init__(self, bus, index, "peripheral")
        self.add_manufacturer_data(0xFFFF, [0x00, 0x01, 0x02, 0x03])
        self.add_service_uuid(ClimateService.CLIMATE_SVC_UUID)
        self.add_local_name("WStation-{}".format(uuid.uuid1().hex))
        self.include_tx_power = False
