import random
import json
from ble.logger import getLogger
from ble.gatt.common import NotPermittedException
from ble.gatt.characteristic import Characteristic
from ble.wstation.descriptors.characteristic_user_description import (
    CharacteristicUserDescriptionDescriptor,
)
from ble.redis import get_client, get_sensor_data

logger = getLogger(__file__)


class TemperatureCharacteristic(Characteristic):
    uuid = "2A6E"
    description = b"Temperature"

    def __init__(self, bus, index, service):
        Characteristic.__init__(
            self,
            bus,
            index,
            self.uuid,
            ["read"],
            service,
        )
        self.add_descriptor(CharacteristicUserDescriptionDescriptor(bus, 3, self))

    def ReadValue(self, options):
        try:
            client = get_client()
            data = get_sensor_data(client)
            return int(data['temperature'] * 100).to_bytes(2, 'little')
        except Exception as e:
            logger.error('Error: {}'.format(e))
        return [0x0]

    def WriteValue(self, value, options):
        raise NotPermittedException
