from ble.gatt.service import Service
from ble.wstation.characteristics.temperature import TemperatureCharacteristic
from ble.wstation.characteristics.humidity import HumidityCharacteristic
from ble.wstation.characteristics.pressure import PressureCharacteristic
from ble.utils import find_sensor_data
from ble.logger import getLogger

logger = getLogger(__file__)


class ClimateService(Service):

    CLIMATE_SVC_UUID = "181A"

    def __init__(self, bus, index):
        self.PATH_BASE = "/org/bluez/sense8/service/climate"
        Service.__init__(self, bus, index, self.CLIMATE_SVC_UUID, True)
        self._sensor_data_path = None
        self.add_characteristic(TemperatureCharacteristic(bus, 1, self))
        self.add_characteristic(HumidityCharacteristic(bus, 2, self))
        self.add_characteristic(PressureCharacteristic(bus, 3, self))
