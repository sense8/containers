import os
import json
import random
from ble.config import base_path

SENSOR_DATA = {
    "mock_sensor": {
        "mock_field": random.randint(0, 100)
    }
}

def make_config(config):
    if not os.path.exists(config["data_path"]):
        config["data_path"] = os.path.join(base_path, "test_data")
    if not os.path.exists(config["data_path"]):
        os.mkdir(config["data_path"])
    return config


def find_sensor_data(sensor_name: str, field: str) -> str:
    # logger.debug('sensor_name: {}'.format(config["senso_name"]))
    # for (dirpath, dirnames, filenames) in os.walk(config["data_path"]):
    #     for fn in filenames:
    #         if fn.startswith("sensor_{}".format(sensor_name)):
    #             return os.path.join(dirpath, fn)

    return SENSOR_DATA[sensor_name][field]


def read_json(json_path):
    with open(json_path, "r", encoding="utf8") as rh:
        return json.load(rh)