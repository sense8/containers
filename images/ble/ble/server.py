# from __future__ import print_function

# import argparse

import dbus
import dbus.exceptions
import dbus.mainloop.glib
import dbus.service
import time

# import threading

from gi.repository import GObject, GLib
import sys
import json
import time
from ble.logger import getLogger
from ble.gatt.common import (
    ADAPTER_IFACE,
    AGENT_MANAGER_IFACE,
    BLUEZ_SERVICE_NAME,
    DBUS_OM_IFACE,
    GATT_MANAGER_IFACE,
    LE_ADVERTISING_MANAGER_IFACE,
)
from ble.gatt.agent import Agent
from ble.gatt.application import Application, register_app_cb, register_app_error_cb
from ble.wstation.advertisement import WstationAdvertisement
from ble.wstation.services.climate import ClimateService
from ble.wstation.common import AGENT_PATH

logger = getLogger(__file__)
mainloop = None


def register_ad_cb():
    logger.debug("Advertisement registered")


def register_ad_error_cb(error):
    logger.debug("Failed to register advertisement: " + str(error))
    mainloop.quit()
    raise error

def find_adapter(bus):
    remote_om = dbus.Interface(bus.get_object(BLUEZ_SERVICE_NAME, "/"), DBUS_OM_IFACE)
    objects = remote_om.GetManagedObjects()

    for o, props in objects.items():
        if LE_ADVERTISING_MANAGER_IFACE in props:
            return o

    return None


def shutdown(timeout):
    logger.debug("Advertising for {} seconds...".format(timeout))
    time.sleep(timeout)
    mainloop.quit()


def serve(config):
    logger.debug('config: {}'.format(config))
    global mainloop

    dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)

    bus = dbus.SystemBus()

    adapter = find_adapter(bus)
    if not adapter:
        logger.debug("BLE adapter not found")
        return

    adapter_obj = bus.get_object(BLUEZ_SERVICE_NAME, adapter)
    adapter_props = dbus.Interface(
        bus.get_object(BLUEZ_SERVICE_NAME, adapter), "org.freedesktop.DBus.Properties"
    )
    adapter_props.Set(ADAPTER_IFACE, "Powered", dbus.Boolean(1))

    svc_manager = dbus.Interface(adapter_obj, GATT_MANAGER_IFACE)
    ad_manager = dbus.Interface(
        bus.get_object(BLUEZ_SERVICE_NAME, adapter), LE_ADVERTISING_MANAGER_IFACE
    )

    wstation_advertisement = WstationAdvertisement(bus, 4)
    obj = bus.get_object(BLUEZ_SERVICE_NAME, "/org/bluez")

    agent = Agent(bus, AGENT_PATH)

    app = Application(bus)
    app.add_service(ClimateService(bus, 2))

    agent_manager = dbus.Interface(obj, AGENT_MANAGER_IFACE)
    agent_manager.RegisterAgent(AGENT_PATH, "NoInputNoOutput")

    mainloop = GLib.MainLoop()

    ad_manager.RegisterAdvertisement(
        wstation_advertisement.get_path(),
        {},
        reply_handler=register_ad_cb,
        error_handler=register_ad_error_cb,
    )

    svc_manager.RegisterApplication(
        app.get_path(),
        {},
        reply_handler=register_app_cb,
        error_handler=[register_app_error_cb],
    )

    agent_manager.RequestDefaultAgent(AGENT_PATH)

    mainloop.run()
    ad_manager.UnregisterAdvertisement(wstation_advertisement)
    logger.debug("Advertisement unregistered")
    dbus.service.Object.remove_from_connection(wstation_advertisement)
