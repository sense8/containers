import unittest
import logging


class TestRedisClient(unittest.TestCase):
    def setUp(self):
        from ble.logger import getLogger
        self.logger = getLogger(__file__)
        logging.basicConfig(
            level=logging.DEBUG,
            format="%(asctime)s [%(levelname)s] %(message)s",
            handlers=[
                logging.StreamHandler(),
            ],
        )
        from ble.redis import get_client
        self.client = get_client()

    def test_get_sensor(self):
        from ble.redis import get_sensor_data
        data = get_sensor_data(self.client)
        self.logger.debug('data: {}'.format(data))

    def test_get_invalid_sensor(self):
        from ble.redis import get_sensor_data
        try:
            get_sensor_data(self.client, sensor_name="foobar", sensor_id=30)
            self.fail('ShouldRaiseException')
        except Exception:
            pass

if __name__ == '__main__':
    unittest.main()