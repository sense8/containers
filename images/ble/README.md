# sense8 / containers / ble


# docker

BluetoothHD must be running

```sh
/usr/libexec/bluetooth/bluetoothd &
```

Advertising must be enabled

```sh
bluetoothctl
advertise on
```

bluetoothctl should be running in the background for advertising registration success.

```sh
docker run --network sense8 -v /var/run/dbus/system_bus_socket:/var/run/dbus/system_bus_socket -v /sense8/containers/ble/:/usr/src/ble -it showi/sense8-ble:0.0.1 /bin/sh
```

## test

When not using docker compose you need to create docker network for container communication with redis

```sh
docker network create sense8
```

The redis could be loaded with test data.

```sh
docker run -d --network sense8 --name redis --expose 6379 -v /sense8/containers/redis/test_data/:/data showi/sense8-redis:0.0.1
```

```sh
python3 -m unittest ble/tests/test_*.py
```


# Links

* [BLE and GATT for IoT: Getting Started with Bluetooth Low Energy and the Generic Attribute Profile Specification for IoT](https://programmaticponderings.com/2020/08/04/getting-started-with-bluetooth-low-energy-ble-and-generic-attribute-profile-gatt-specification-for-iot/)