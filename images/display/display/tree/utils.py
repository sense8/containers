from display.tree.node import Node
from display.logger import getLogger

logger = getLogger(__file__)


class Stats:
    def __init__(self, stats={"total": 0}):
        self.stats = dict(stats)

    def inc(self, name, increment=1):
        self.stats[name] += 1

    def __getattr__(self, key):
        return self.stats[key]


def traverse_empty_callback(node: Node, *a, **ka):
    logger.debug("[traverse] {}".format(node.o("name")))


def traverse_dfs(
    root: Node = None, callback: callable = traverse_empty_callback, *a, **ka
):
    for child in root.childs:
        traverse_dfs(child, callback=callback, *a, **ka)
        callback(child, *a, **ka)
    callback(root, *a, **ka)


def traverse_bfs(
    root: Node = None, callback: callable = traverse_empty_callback, *a, **ka
):
    if not callback(root, *a, **ka):
        return False
    for child in root.childs:
        if callback(child, *a, **ka):
            traverse_bfs(child, callback=callback, *a, **ka)


def down_dirty(root, dirty=True):
    traverse_bfs(
        root, callback=lambda node: True if setattr(node, "dirty", dirty) else True
    )
