import time

import luma.core
from display.tree.layer import LayerManager
from display.tree.node import Node
from luma.core.sprite_system import framerate_regulator
from display.conf import conf


class RenderContext:
    def __init__(self, device=None, tree=Node, cache_enabled=True):
        self.device = device
        self.tree = tree
        self.started_on = time.time()
        self.elapsed = 0
        self.layers = LayerManager(ctx=self)
        self.cache_enabled = cache_enabled
        self.regulator = framerate_regulator(fps=conf["fps"])

    def __getattr__(self, k):
        raise KeyError(k)
