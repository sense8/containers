import time
import luma.core
from PIL import ImageDraw
from display.utlils import DictByKeyAccess
from display.logger import getLogger
from display.tree.utils import traverse_dfs, traverse_bfs, Stats
from display.tree.node import Node
from display.color import colors
from display.tree.context import RenderContext

logger = getLogger(__file__)


def update_tree(ctx: RenderContext):
    def callback(node: Node, ctx: RenderContext):
        if not node.active:
            return False
        if hasattr(node, "update"):
            node.update(ctx=ctx)
        return True

    traverse_bfs(ctx.tree, ctx=ctx, callback=callback)


def render_tree(ctx: RenderContext, draw=ImageDraw):
    logger.debug("render")

    def callback(node: Node, ctx: RenderContext):
        if not node.active:
            return False
        if hasattr(node, "draw"):
            img = None
            if not ctx.cache_enabled or node.dirty is True:
                node._render_cache = node.draw(ctx=ctx)
                logger.debug("draw {}".format(node.o("name")))
                node.dirty = False
            ctx.layers.compose(node._render_cache)
        return True

    traverse_bfs(ctx.tree, ctx=ctx, callback=callback)
    img = ctx.layers.render()
    ctx.layers.reset()
    return img


class Renderer:
    def __init__(self, ctx: RenderContext = None):
        self.ctx = ctx

    def update(self):
        ctx = self.ctx
        ctx.started_on = time.time()
        ctx.alive = True
        last_time = ctx.started_on
        while ctx.alive:
            current_time = time.time()
            ctx.elapsed = current_time - last_time
            last_time = current_time
            update_tree(ctx)
            yield ctx
        yield None

    def draw(self, ctx=None, draw=None):
        return render_tree(ctx=ctx, draw=draw)
