from display.tree.node import Node, merge_dict
from display.font import make_font
from display.redis import make_client
from display.logger import getLogger
import json
import random

logger = getLogger(__file__)


class Switch(Node):
    __allowed_options__ = {"method": str}

    def __init__(self, **options):
        options = merge_dict(
            options,
        )
        self.selected = None
        super(Switch, self).__init__(**options)

    @property
    def dirty(self):
        return True

    @dirty.setter
    def dirty(self, value):
        pass

    def add_child(self, node):
        name = node.o("name")
        node.active = False
        if self.selected is None:
            self.selected = node
            node.active = True
            node.dirty = True
        super(Switch, self).add_child(node)

    def update(self, ctx=None):
        self.dirty = True

    def select(self, selected_name):
        for child in self.childs:
            name = child.o("name")
            if name == selected_name:
                self.selected = child
                child.active = True
                child.dirty = True
            else:
                child.active = False
        logger.debug("Selected: {}".format(self.selected.o("name")))


class RedisSwitch(Switch):
    def __init__(self, channels={}):
        super(RedisSwitch, self).__init__(method="redis")
        self.thread = None
        self.channels = channels
        self.start()

    def start(self):
        self.stop()
        self.redis = make_client(
            channels={
                channel: handler(self) for channel, handler in self.channels.items()
            }
        )
        self.thread = self.redis.pubsub.run_in_thread(sleep_time=1)

    def stop(self):
        if self.thread is None:
            return
        try:
            self.thread.stop()
            self.thread = None
        except Exception as e:
            logger.error(e)
