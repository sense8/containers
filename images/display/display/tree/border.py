from PIL import Image, ImageSequence, ImageDraw, ImageFont
from luma.core.render import canvas
from display.tree.node import Node, merge_dict, getopt
from display.mode import modes
from display.color import colors
from display.font import make_font
from display.logger import getLogger

logger = getLogger(__file__)


class Border(Node):
    __allowed_options__ = {
        "size": [int, int],
        "fill": str,
        "position": [int, int],
    }

    def __init__(self, **options):
        options = merge_dict({"fill": "white", "position": (0, 0), "size": 2}, options)
        super(Border, self).__init__(**options)

    def draw(self, ctx=None, draw=None):
        size = ctx.device.size
        width = self.o("size")
        img = Image.new(modes.RGBA, size, (1, 1, 1, 1))
        img.zindex = 5
        pos = self.o("position")
        draw = ImageDraw.ImageDraw(img)
        draw.rectangle((pos, size), fill=(1, 1, 1, 0))
        draw.rectangle(
            ((pos[0] + width, pos[1] + width), (size[0] - width, size[1] - width)),
            fill=(0, 0, 0, 0),
        )
        return img
