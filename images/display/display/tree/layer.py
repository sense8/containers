import luma.core
from display.color import alpha_colors
from display.logger import getLogger
from display.mode import modes
from display.tree.node import Node
from display.utlils import clamp
from PIL import Image, ImageDraw, ImageFont, ImageSequence
from pyllist import dllist, dllistnode

logger = getLogger(__file__)

ZINDEX_DEFAULT = 10


def merge_layers(layers=[]):
    layers = list(layers)
    base = layers.pop().image
    for layer in layers:
        base = Image.alpha_composite(base, layer.image)
    return base


class Layer:
    def __init__(self, ctx, zindex=10):
        self.ctx = ctx
        self.zindex = clamp(zindex, 0, 20)
        self.reset()

    @property
    def image(self):
        return self._image

    @image.setter
    def image(self, image):
        self._image = image
        self._image.zindex = self.zindex

    def reset(self):
        self.image = Image.new(modes.RGBA, self.ctx.device.size, alpha_colors["black"])

    def compose(self, foreground: Image):
        self.image = Image.alpha_composite(self.image, foreground)

    @property
    def width(self):
        return self.image.size[0]

    @property
    def height(self):
        return self.image.size[1]

    @property
    def mode(self):
        return self.image.mode

    @property
    def size(self):
        return self.image.size

    def display(self, image=None):
        if image is not None:
            self.compose(image)
        return self.image


class LayerManager:
    def __init__(self, ctx):
        self.ctx = ctx
        self.layers = dllist()

    def compose(self, img: Image):
        if img is None:
            return
        zindex = ZINDEX_DEFAULT
        if hasattr(img, "zindex"):
            zindex = img.zindex
        destination = None
        if not self.layers.size:
            destination = Layer(self.ctx, zindex=zindex)
            self.layers.appendright(destination)
        else:
            for node in self.layers.iternodes():
                layer = node.value
                if zindex < layer.zindex:
                    destination = Layer(self.ctx, zindex=zindex)
                    self.layers.appendleft(destination)
                    break
                elif layer.zindex == zindex:
                    destination = layer
                    break
        if destination is None:
            destination = Layer(self.ctx, zindex=zindex)
            self.layers.appendright(destination)
        destination.compose(img)

    def render(self):
        base = None
        for node in self.layers.iternodes():
            layer = node.value
            if base is None:
                base = layer.image
            else:
                base = Image.alpha_composite(base, layer.image)
        return base

    def reset(self):
        self.layers = dllist()
