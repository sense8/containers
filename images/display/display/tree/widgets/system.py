import os
from display.tree.node import Node, merge_dict
from display.tree.switch import Switch
from display.tree.text import Text
from display.tree.border import Border
from display.font import make_font
from display.redis import make_client
from display.logger import getLogger
import json
import random

logger = getLogger(__file__)

uname_fields_name = ("sysname", "nodename", "release", "version", "machine")
uname_info_raw = os.uname()
uname_info = {
    uname_fields_name[idx]: uname_info_raw[idx]
    for idx in range(0, len(uname_fields_name))
}
make_uname_fmt_values = lambda node: [uname_info[k] for k in ["sysname"]]


class SystemWidget(Node):
    __allowed_options__ = {}

    def __init__(self, **options):
        options = merge_dict(
            options,
        )
        super(SystemWidget, self).__init__(**options)
        self.font = make_font("code2000", 14)
        text = Text(
            fmt="{}",
            values=make_uname_fmt_values,
            position=(0, 0),
            font=self.font,
        )
        text.add_child(Border())
        self.add_child(text)
