from display.tree.node import Node
from display.tree.switch import Switch
from display.tree.text import Text
from display.font import make_font
from display.redis import make_client
from display.logger import getLogger
import json
import random

logger = getLogger(__file__)


class TestWidget(Node):
    __allowed_options__ = {}

    def __init__(self):
        super(TestWidget, self).__init__()
        self.font = make_font("code2000", 14)
        self.temperature = -1
        self.humidity = -1
        self.add_child(
            Text(
                fmt=" {} °C ",
                values=lambda node: self.temperature,
                position=(0, 0),
                font=self.font,
            )
        )
        self.add_child(
            Text(
                fmt=" {} %RH ",
                values=lambda node: self.humidity,
                position=(64, 0),
                font=self.font,
            )
        )

        def handler(obj):
            def inner(msg):
                data = None
                try:
                    data = json.loads(msg["data"])
                except Exception as e:
                    logger.error(e)
                    return
                obj.temperature = data["temperature"]
                obj.humidity = data["humidity"]

            return inner

        self.redis = make_client(channels={"climate": handler(self)})
        self.redis.pubsub.run_in_thread(sleep_time=0.01)
