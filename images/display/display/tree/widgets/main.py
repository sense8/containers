import json
import random

from display.logger import getLogger
from display.tree.node import Node
from display.tree.switch import RedisSwitch
from display.tree.widgets.climate import ClimateWidget
from display.tree.widgets.system import SystemWidget

logger = getLogger(__file__)


class MainWidget(Node):
    __allowed_options__ = {}

    def __init__(self, **options):
        super(MainWidget, self).__init__(**options)

        def handle(node):
            def inner(msg):
                keys = [k.strip() for k in msg["data"].split(" ")]
                filtered = []
                for key in keys:
                    for child in node.childs:
                        name = child.o("name")
                        if key != name:
                            continue
                        node.select(name)
                        return

            return inner

        switch = RedisSwitch(channels={"display.widget.show": handle})
        for widget in [ClimateWidget, SystemWidget]:
            switch.add_child(widget())
        self.add_child(switch)

    @property
    def dirty(self):
        return True

    @dirty.setter
    def dirty(self, value):
        pass