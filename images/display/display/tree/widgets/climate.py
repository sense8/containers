from display.tree.node import Node, merge_dict
from display.tree.switch import Switch
from display.tree.text import Text
from display.tree.border import Border
from display.tree.utils import down_dirty
from display.font import make_font
from display.redis import make_client
from display.logger import getLogger
import json
import random

logger = getLogger(__file__)


class ClimateWidget(Node):
    __allowed_options__ = {"name": str}

    @property
    def dirty(self):
        return True

    @dirty.setter
    def dirty(self, value):
        pass

    def __init__(self, **options):
        super(ClimateWidget, self).__init__(**options)
        self.font = make_font("code2000", 14)
        self.temperature = -1
        self.humidity = -1
        temperature = Text(
            fmt=" {} °C ",
            values=lambda node: self.temperature,
            position=(0, 0),
            font=self.font,
        )
        # temperature.add_child(Border())
        self.add_child(temperature)
        # humidity = Text(
        #     fmt=" {} %RH ",
        #     values=lambda node: self.humidity,
        #     position=(0, 0),
        #     font=self.font,
        # )
        # # humidity.add_child(Border())
        # self.add_child(humidity)

        def handle(node):
            def inner(msg):
                data = None
                try:
                    data = json.loads(msg["data"])
                except Exception as e:
                    logger.error(e)
                    return
                node.set_temperature(data["temperature"])
                node.humidity = data["humidity"]

                logger.debug("t: {}, h: {}".format(node.temperature, node.humidity))
                down_dirty(node)

            return inner

        self.redis = make_client(channels={"climate": handle(self)})
        self.redis.pubsub.run_in_thread(sleep_time=1)
        logger.debug(self.redis)

    def set_temperature(self, temperature):
        self.temperature = temperature
        logger.debug("Set temp: {}".format(self.temperature))
