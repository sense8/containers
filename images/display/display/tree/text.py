import math

from display.color import colors
from display.font import make_font
from display.logger import getLogger
from display.utlils import clamp
from display.mode import modes
from display.tree.layer import Layer
from display.tree.node import Node, getopt, merge_dict
from luma.core.render import canvas
from luma.core.virtual import hotspot, terminal, viewport
from PIL import Image, ImageDraw, ImageFont, ImageSequence

logger = getLogger(__file__)


class Hotspot(hotspot):
    def __init__(self, size, layer=None, *a, **ka):
        self.layer = layer
        super(Hotspot, self).__init__(*size)

    def update(self, draw=None):
        pass

    def should_redraw(self):
        return True

    def display(self, img=None):
        return img
        # self.layer.compose(img)
        # return self.layer.display()


# class Viewport(viewport):
#     def display(self, img=None):
#         logger.debug(self.__dict__.keys())
#         return self._backing_image


def make_snapshot(width, height, text, fonts, color="white"):
    def render(draw, width, height):
        t = text

        for font in fonts:
            size = draw.multiline_textsize(t, font)
            if size[0] > width:
                t = text.replace(" ", "\n")
                size = draw.multiline_textsize(t, font)
            else:
                break

        left = (width - size[0]) // 2
        top = (height - size[1]) // 2
        draw.multiline_text(
            (left, top), text=t, font=font, fill=color, align="center", spacing=-2
        )

    return snapshot(width, height, render, interval=10)


class Text(Node):
    __allowed_options__ = {
        "text": str,
        "fill": str,
        "position": [int, int],
        "font": object,
        "fmt": str,
        "values": [],
        "size": [int, int],
        "speed": float,
    }

    def __init__(self, **options):
        options = merge_dict(
            {
                "fill": "white",
                "position": (0, 0),
                "font": make_font(),
                "size": None,
                "speed": 2,
            },
            options,
        )
        super(Text, self).__init__(**options)
        self.view = None
        self._text = None
        self._idx = 0

    def make_text(self):
        if "fmt" in self.options and self.options["fmt"]:
            return self.options["fmt"].format(self.make_values())
        return self.options["text"]

    def make_values(self):
        values = self.options["values"]
        if callable(values):
            result = values(self)
            return result
        return values

    def update(self, ctx=None):
        if not self.view:
            self.layer = Layer(ctx=ctx)
            self.view = viewport(
                self.layer,
                ctx.device.size[0],
                ctx.device.size[1],
                modes.RGBA,
                dither=True,
            )
            self.term = terminal(self.view, animate=False)
        new_text = self.make_text()
        if self._text is None or self._text != new_text:
            # logger.debug("NewText: {}".format(new_text))
            self._text = new_text
            # self._idx = 0
            self.layer.reset()
            self.term.clear()
            # self.term.puts(new_text)
            # self.dirty = True
        # if self._idx <= len(self._text):
        #     self.dirty = True

    def draw(self, ctx=None, draw=None):
        self.term.puts(self._text)
        # count = clamp(int(ctx.elapsed * self.o("speed")), 1, len(self._text))
        # self.term.puts(self._text[self._idx : count])
        # self._idx += count
        #  self.term.flush()
        return self.layer.display()
