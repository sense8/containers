from PIL import Image, ImageSequence, ImageDraw, ImageFont
from display.logger import getLogger
from display.mode import modes
from display.color import colors
from display.utlils import merge_dict, camel_to_snake

logger = getLogger(__file__)
__default_node_options__ = {"name": None}


def check_node_options(node, options={}):
    if options is None or not len(options):
        return True
    for key in options:
        if key in __default_node_options__:
            continue
        if key in node.__allowed_options__:
            continue
        raise KeyError(key)
    if "name" not in options or options["name"] is None:
        options["name"] = camel_to_snake(node.__class__.__name__)


def getopt(node, key, default=None):
    if key not in node.options:
        raise KeyError(key)
    value = node.options[key]
    if value is None:
        return default
    return value


class Node:
    class MissingParameterError(Exception):
        pass

    def __init__(self, **options):
        self.dirty = True
        self.active = True
        self.options = merge_dict(__default_node_options__, options)
        check_node_options(self, self.options)
        self.childs = []

    def o(self, key, default=None):
        """get option by key"""
        return getopt(self, key, default=default)

    def add_child(self, node):
        self.childs.append(node)
