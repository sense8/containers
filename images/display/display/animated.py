import os
from luma.core.sprite_system import framerate_regulator
from luma.core import cmdline, error
from PIL import Image
from display import get_base_path


class Animated:
    def __init__(self, device, name):
        self.name = name
        self.image_path = get_base_path("images", "{}.gif".format(name))
        self.setup(device)

    def setup(self, device):
        self.regulator = framerate_regulator(fps=10)
        self.image = Image.open(self.image_path)
        self.size = [min(*device.size)] * 2
        # self.posn = ((device.width - self.size[0]) // 2, device.height - self.size[1])
        self.posn = (0, device.height - self.size[1])