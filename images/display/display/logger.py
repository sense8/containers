import logging
import os
from display import get_base_path


def getLogger(file_path):
    name = file_path[len(get_base_path()) :]
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)
    return logger
