from PIL import ImageFont
from display import get_base_path
from display.utlils import ArrayByKeyAccess


make_font = lambda name="pixelmix", size=8, ext="ttf": ImageFont.truetype(
    get_base_path("fonts", "{name}.{ext}".format(name=name, ext=ext)), size
)

fonts = ArrayByKeyAccess(["code2000", "FreeMono", "pixelmix"])
