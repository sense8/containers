from display import get_base_path
from display.utlils import read_json, merge_dict


def make_conf(**kw):
    return merge_dict({}, read_json(get_base_path("etc", "display.json")))


conf = make_conf()