import os


def get_base_path(*args):
    return os.path.join(
        os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)), *args
    )
