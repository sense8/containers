import time
import os
import json
import re
from display.logger import getLogger

logger = getLogger(__name__)


class ArrayByKeyAccess:
    def __init__(self, array):
        self._dct = {k: True for k in array}

    def __getattr__(self, k):
        return k if self._dct[k] else None


class DictByKeyAccess:
    def __init__(self, dct):
        self._dct = dct

    def __getattr__(self, k):
        if hasattr(self, k):
            return getattr(self._dct, k)

    def __setattr__(self, k, v):
        setattr(self._dct, k, v)


def its_time(timeout=3, callback=lambda: False):
    def loop(timeout=None, callback=None):
        started_on = time.time()
        next_time = 0
        while True:
            ctime = time.time()
            if ctime > next_time:
                try:
                    callback()
                except Exception as e:
                    logger.error("Error: {}".format(e))
                next_time = ctime + timeout
            yield next_time

    return loop(timeout=timeout, callback=callback)


def read_json(file_path: str) -> object:
    with open(file_path, "r", encoding="utf8") as rh:
        return json.load(rh)


def merge_dict(*args):
    f = {}
    for dct in args:
        f.update(dct)
    return f


camel_to_snake_pattern = re.compile(r"(?<!^)(?=[A-Z])")
camel_to_snake = lambda camel: camel_to_snake_pattern.sub("_", camel).lower()

clamp = lambda value, vmin, vmax: max(min(value, vmax), vmin)