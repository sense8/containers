import argparse


def parse_args():
    parser = argparse.ArgumentParser(description="sense8/display cmdline arguments")
    parser.add_argument("--emulated", action="store_true")

    return parser.parse_args()
