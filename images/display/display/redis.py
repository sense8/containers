import redis
import time
from display.conf import conf
from display.logger import getLogger

_client = None
logger = getLogger(__file__)

redis_conf_default = {"host": "redis", "port": 6389, "db": 0}


class RedisContext:
    def __init__(self, client=None, channels={}):
        conf = self.make_conf()
        if client is None:
            client = redis.Redis(**conf["server"], decode_responses=True)
        self.client = client
        self.client.set("display.running", time.time())
        self.pubsub = None
        if len(channels.keys()):
            self.pubsub = self.client.pubsub()
            for channel, handler in channels.items():
                self.pubsub.psubscribe(**{channel: handler})

    def make_conf(self):
        srv_conf = dict(redis_conf_default)
        srv_conf.update(conf["redis"])
        self.conf = {"timeout": 15000, "server": srv_conf}
        return self.conf

    @property
    def client(self):
        self.wait(timeout=self.conf["timeout"])
        return self._client

    @client.setter
    def client(self, client):
        self._client = client

    def ping(self, client=None):
        if client is None:
            client = self.client
        client.set("display.running", time.time())

    def wait(self, timeout=15000):
        mode = 0
        if timeout is None:
            timeout = 0
        elif timeout == 0:
            mode = 1
            timeout = 0
        else:
            mode = 2
        wait_until = time.time() + timeout
        while True:
            try:
                self.ping(client=self._client)
                return True
            except Exception as e:
                if mode == 0:
                    raise e
                elif mode == 2 and time.time() >= wait_until:
                    raise e
                logger.warn(e)
            time.sleep(1)
        raise Exception("Timeout")


def make_client(channels={}):
    return RedisContext(channels=channels)
