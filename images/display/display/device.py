from luma.core import cmdline, error


class DeviceManager:
    def __init__(
        self,
        emulated: bool = False,
        size=(128, 32),
        mode: str = "RGBA",
        serial=None,
        hw_device=None,
    ):
        self.size = size
        self.mode = mode
        self.emulated = emulated
        self._device = None
        self.hw_device = hw_device

    def get(self):
        if not self._device:
            if self.emulated:
                import os
                from luma.emulator.device import pygame

                width, height = self.size
                os.environ["SDL_VIDEO_WINDOW_POS"] = "%d,%d" % (0, 0)
                self._device = pygame(
                    width=width,
                    height=height,
                    rotate=0,
                    mode=self.mode,
                    transform="none",
                    scale=10,
                )
                self._device.show()
            else:
                self._device = self.hw_device
        return self._device
