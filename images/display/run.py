#!/usr/bin/env python3

import os
import sys
import time

module_path = os.path.abspath(os.path.join(os.path.dirname(__file__)))
sys.path.insert(0, module_path)

if __name__ == "__main__":
    import logging
    import os

    from luma.core.render import canvas
    from display import get_base_path
    from display.conf import conf
    from display.device import DeviceManager
    from display.logger import getLogger
    from display.opts import parse_args
    from display.tree.renderer import RenderContext, Renderer
    from display.tree.widgets.main import MainWidget

    logging.basicConfig()
    logger = getLogger(__file__)
    args = parse_args()
    hw_device = None
    serial = None

    if not args.emulated:
        from luma.core.interface.serial import i2c
        from luma.oled.device import ssd1306

        serial = i2c(port=1)  # , address=0x3C)
        hw_device = ssd1306(serial, height=32)
    else:
        conf["redis"].update({"host": "127.0.0.1"})
    deviceManager = DeviceManager(
        emulated=args.emulated, size=(128, 32), serial=serial, hw_device=hw_device
    )
    device = deviceManager.get()
    tree = MainWidget()
    ctx = RenderContext(device=device, tree=tree)
    renderer = Renderer(ctx)
    with canvas(device) as draw:
        for ctx in renderer.update():
            # logger.debug("elapsed: {}".format(ctx.elapsed))
            img = renderer.draw(draw=draw, ctx=ctx)
            with ctx.regulator:
                device.display(img.convert(device.mode))
