# Sense8 - Supervisor

Simple Docker image that that act as fake device for testing `ansible-pull` and `docker-compose`.

Device must have ansible and git installed.

This docker file run docker-compose by mounting `/var/run/docker.sock` so containers are started on the same host.

See [Makefile](Makefile)

## Links

* [Telegraf - configuration](https://github.com/influxdata/telegraf/blob/master/docs/CONFIGURATION.md)
* [Run Docker containers on embedded devices](https://www.balena.io)
* [Telegraf / InfluxDB / Grafana on RaspberryPi – From Scratch](https://nwmichl.net/2020/07/14/telegraf-influxdb-grafana-on-raspberrypi-from-scratch/)
* [Creating a BLE Peripheral with BlueZ](https://punchthrough.com/creating-a-ble-peripheral-with-bluez/)
* [Building Minimal Docker Containers for Python Applications](vhttps://blog.realkinetic.com/building-minimal-docker-containers-for-python-applications-37d0272c52f3)
* [BLE and GATT for IoT](https://itnext.io/ble-and-gatt-for-iot-2ae658baafd5)
* [Guide for BME680 Environmental Sensor with Arduino (Gas, Temperature, Humidity, Pressure)](https://randomnerdtutorials.com/bme680-sensor-arduino-gas-temperature-humidity-pressure/)
* [Pimoroni / BME680 / Air quality](https://github.com/pimoroni/bme680-python/blob/master/examples/indoor-air-quality.py)
* [How to connect container to DBus from host](https://github.com/mviereck/x11docker/wiki/How-to-connect-container-to-DBus-from-host)
