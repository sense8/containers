FROM showi/sense8-base-image:0.0.1 as image

FROM image as build
RUN apk add --no-cache --virtual .build-deps \
    go \
    ca-certificates \
    make \
    git \
    linux-headers \
    gcc \
    libc-dev
WORKDIR /build
RUN git clone https://github.com/influxdata/telegraf.git
WORKDIR /build/telegraf
RUN git checkout tags/v1.17.0
RUN make telegraf
WORKDIR  /build
RUN git clone https://gitlab.com/sense8/goget-redis.git
WORKDIR /build/goget-redis/main
RUN go build -o ../bin/redis-get
RUN apk del --no-network .build-deps

FROM image
WORKDIR /
COPY --from=build /usr/lib/ /usr/lib
COPY --from=build /usr/bin /usr/bin
COPY --from=build /build/telegraf/telegraf /bin
COPY --from=build /build/goget-redis/bin/redis-get /bin/redis-get
LABEL maintainer="showi"
LABEL description="alpine-3.12.3, telegraf-1.17.0 redis-get"
COPY etc/ /etc

CMD ["telegraf"]