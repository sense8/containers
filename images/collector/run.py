#!/usr/bin/env python3

import sys
import os

module_path = os.path.abspath(os.path.join(os.path.dirname(__file__)))

sys.path.insert(0, module_path)

if __name__ == "__main__":
    import logging
    import json
    import asyncio

    logging.basicConfig(
        format="%(asctime)s,%(msecs)d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s"
    )
    from collector.logger import getLogger

    logger = getLogger(__file__)

    from collector.config import get_config

    config = get_config()
    from collector.sensors.plugin import load_plugin
    from collector.collect import collect
    from collector.healthcheck import healthcheck

    def extract_sensor_name_and_id(line):
        if ":" in line:
            name, sid = line.split(":")
            return name, int(sid)
        return line, 0

    sensors = []
    for line in config["sensor_plugins"]:
        sensor_name, sensor_id = extract_sensor_name_and_id(line)
        logger.debug(
            "sensor_name: {sensor_name}, sensor_id: {sensor_id}".format(
                sensor_name=sensor_name, sensor_id=sensor_id
            )
        )
        try:
            plugin = load_plugin(sensor_name)
            sensor = plugin.PluginSensor(config, sensor_id=sensor_id)
            sensor.setup()
            sensors.append(sensor)
            logger.debug("Registered sensor: {}".format(sensor))
        except Exception as e:
            logger.error("PluginImportError: {}".format(e))
    # logger.debug(json.dumps(config, indent=4))
    tasks = list()
    loop = asyncio.get_event_loop()
    tasks.append(loop.create_task(collect(config, sensors)))
    # tasks.append(loop.create_task(healthcheck()))
    try:
        loop.run_until_complete(asyncio.wait(tasks))
    except KeyboardInterrupt:
        pass
    loop.run_until_complete(asyncio.wait(tasks))
    loop.close()