import os
import datetime
import json
from collector.config import base_path


def get_env(name, default_value=None) -> str:
    value = os.environ.get(name)
    if value is None:
        return str(default_value)
    return value


def make_timestamp() -> str:
    return datetime.datetime.now().isoformat()


def make_timestamp_epoch() -> float:
    return datetime.datetime.now().timestamp()


def write(write_path, data_str) -> None:
    with open(write_path, "w", encoding="utf8") as wh:
        wh.write(data_str)


def make_data(**ka) -> str:
    if "timestamp" not in ka:
        ka["timestamp"] = make_timestamp()
    return json.dumps(ka, indent=None)


def make_config(config) -> dict:
    if not os.path.exists(config["data_path"]):
        config["data_path"] = os.path.join(base_path, "data")
    if not os.path.exists(config["data_path"]):
        os.mkdir(config["data_path"])
    config.update(
        {
            "location": get_env("WS_LOCATION", "paris"),
            "room": get_env("WS_ROOM", "chambre"),
        }
    )
    return config


def read_json(json_path) -> dict:
    with open(json_path, "r", encoding="utf8") as rh:
        return json.load(rh)