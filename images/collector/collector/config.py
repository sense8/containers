import os
from collector import base_path
from collector.utils import read_json
from collector.logger import getLogger

logger = getLogger(__file__)

base_config_dir = "/etc/sense8"
config_file = os.path.join(base_config_dir, "collector.json")
local_data_path = os.path.join(base_path, "data")


def get_config():
    conf = read_json(config_file)
    if not os.path.exists(conf["data_path"]):
        conf["data_path"] = local_data_path
        if not os.path.exists(conf["data_path"]):
            os.mkdir(conf["data_path"])
    return conf
