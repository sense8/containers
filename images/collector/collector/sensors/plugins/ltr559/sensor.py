from ltr559 import LTR559
from collector.logger import getLogger
from collector.sensors.sensor_base import Sensor
from collector.utils import make_timestamp, write, make_data

logger = getLogger(__file__)


class PluginSensor(Sensor):
    def __init__(self, config, **ka):
        ka.update({"name": "lux_proximity", "sensor_name": "ltr559"})
        super(PluginSensor, self).__init__(config, **ka)

    def _make_sensor(self):
        self.sensor = LTR559()
        return self.sensor

    def setup(self):
        sensor = self._make_sensor()

    async def collect(self):
        sensor = self.sensor
        sensor.update_sensor()
        self.write(
            self.make_common_data(
                lux=sensor.get_lux(), proximity=sensor.get_proximity()
            ),
        )
