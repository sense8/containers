import uuid
import random
import psutil
from collector.logger import getLogger
from collector.sensors.sensor_base import make_sensor_write_path, Sensor
from collector.utils import make_timestamp, write, make_data
from collector import DEBUG

logger = getLogger(__file__)


class PluginSensor(Sensor):
    def __init__(self, config, **ka):
        ka.update(
            {
                "name": "psutil",
                "sensor_name": "python-psutil",
            }
        )
        super(PluginSensor, self).__init__(config, **ka)

    def setup(self):
        pass

    async def collect(self):
        if DEBUG:
            logger.debug("Collect {}".format(self))
        self.write(
            self.make_common_data(
                value=random.randint(0, 100),
            ),
        )
        self.collect_id += 1
