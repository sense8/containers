import bme680
from collector.logger import getLogger
from collector.sensors.sensor_base import make_sensor_write_path, Sensor
from collector.utils import make_timestamp, make_timestamp_epoch, write, make_data

logger = getLogger(__file__)


class PluginSensor(Sensor):
    def __init__(self, config, **ka):
        ka.update(
            {
                "name": "climate",
                "sensor_name": "bme680",
                "burnin_data": [],
                "burnin_time": 5 * 60,
                "burnin_measures": 50,
                # Set the humidity baseline to 40%, an optimal indoor humidity.
                "humidity_baseline": 40.0,
                # This sets the balance between humidity and gas reading in the
                # calculation of air_quality_score (25:75, humidity:gas)
                "humidity_weighting": 0.25,
                "gas_baseline": None,
            }
        )
        super(PluginSensor, self).__init__(config, **ka)

    def _make_sensor(self):
        sensor = None
        try:
            sensor = bme680.BME680(bme680.I2C_ADDR_PRIMARY)
        except IOError:
            sensor = bme680.BME680(bme680.I2C_ADDR_SECONDARY)
        self.sensor = sensor
        return sensor

    def setup(self):
        sensor = self._make_sensor()
        sensor.set_humidity_oversample(bme680.OS_2X)
        sensor.set_pressure_oversample(bme680.OS_4X)
        sensor.set_temperature_oversample(bme680.OS_8X)
        sensor.set_filter(bme680.FILTER_SIZE_3)
        sensor.set_gas_status(bme680.ENABLE_GAS_MEAS)
        sensor.set_gas_heater_temperature(320)
        sensor.set_gas_heater_duration(150)
        sensor.select_gas_heater_profile(0)

    def make_air_quality(self):
        sensor = self.sensor
        current_time = make_timestamp_epoch()
        if self.gas_baseline is None:
            logger.debug("burnin air quality data")
            if (
                current_time - self.started_on < self.burnin_time
                and len(self.burnin_data) < self.burnin_measures
            ):
                if sensor.get_sensor_data() and sensor.data.heat_stable:
                    self.burnin_data.append(sensor.data.gas_resistance)
            else:
                self.gas_baseline = sum(self.burnin_data[-self.burnin_measures :]) / (
                    self.burnin_measures * 1.0
                )
            return None

        gas = sensor.data.gas_resistance
        gas_offset = self.gas_baseline - gas
        hum = sensor.data.humidity
        hum_offset = hum - self.humidity_baseline
        hum_score = 0
        gaz_score = 0

        # Calculate hum_score as the distance from the hum_baseline.
        if hum_offset > 0:
            hum_score = 100.0 - self.humidity_baseline - hum_offset
            hum_score /= 100.0 - self.humidity_baseline
            hum_score *= self.humidity_weighting * 100.0

        else:
            hum_score = self.humidity_baseline + hum_offset
            hum_score /= self.humidity_baseline
            hum_score *= self.humidity_weighting * 100.0

        # Calculate gas_score as the distance from the gas_baseline.
        if gas_offset > 0:
            gas_score = gas / self.gas_baseline
            gas_score *= 100.0 - (self.humidity_weighting * 100.0)

        else:
            gas_score = 100.0 - (self.humidity_weighting * 100.0)

        # Calculate air_quality_score.
        return hum_score + gas_score

    async def collect(self):
        sensor = self.sensor
        if not sensor.get_sensor_data():
            return False
        air_quality = self.make_air_quality()
        self.write(
            self.make_common_data(
                temperature=sensor.data.temperature,
                pressure=sensor.data.pressure,
                humidity=sensor.data.humidity,
                gas_resistance=sensor.data.gas_resistance,
                air_quality=air_quality,
            ),
        )
