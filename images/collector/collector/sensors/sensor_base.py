import os
import inspect
from collector.utils import (
    make_data,
    write,
    make_timestamp,
    read_json,
    make_timestamp_epoch,
)
from collector.logger import getLogger
from collector.config import base_config_dir
from collector.redis import get_redis_client

logger = getLogger(__file__)


def make_sensor_write_path(sensor):
    return os.path.join(
        sensor.config["data_path"],
        "sensor_{sensor_uid}_{location}_{room}.json".format(
            location=sensor.config["location"],
            room=sensor.config["room"],
            sensor_uid=make_sensor_uid(sensor),
        ),
    )


def make_sensor_uid(sensor):
    return "{sensor_name}.{sensor_id}".format(
        sensor_name=sensor.sensor_name, sensor_id=sensor.sensor_id
    )


def make_sensor_meta_etc(sensor):
    return "sensor.{sensor_uid}.json".format(sensor_uid=make_sensor_uid(sensor))


def read_sensor_meta(sensor):
    sensor_path = inspect.getfile(sensor.__class__)
    meta_path = os.path.join(os.path.dirname(sensor_path), "sensor.meta.json")
    if not os.path.exists(meta_path):
        raise Exception("MissingSensorMetaError<{}>".format(sensor.sensor_name))
    meta = read_json(meta_path)
    etc_meta_path = os.path.join(base_config_dir, make_sensor_meta_etc(sensor))
    if os.path.exists(etc_meta_path):
        meta.update(read_json(etc_meta_path))
    return meta


def add_sensor_meta_to_config(config, sensor):
    if "sensors" not in config:
        config["sensors"] = {}
    if sensor.sensor_name not in config["sensors"]:
        config["sensors"][sensor.sensor_name] = {}
    if sensor.sensor_id in config["sensors"][sensor.sensor_name]:
        logger.error("NUIH")
        raise Exception(
            "SensorWithSameSensorIdError<sensor_name: {sensor_name}, sensor_id: {sensor_id}>".format(
                sensor_name=sensor.sensor_name, sensor_id=sensor.sensor_id
            )
        )
    config["sensors"][sensor.sensor_name][sensor.sensor_id] = sensor.meta


class Sensor:
    def __init__(self, config, **ka):
        self.config = config
        if "sensor_id" not in ka:
            ka["sensor_id"] = 0
        if "name" not in ka:
            ks["name"] = ka["sensor_name"]
        [setattr(self, k, ka[k]) for k in ka]
        self.started_on = make_timestamp_epoch()
        self.next_collect = 0
        self.meta = read_sensor_meta(self)
        add_sensor_meta_to_config(config, self)
        self.setup()

    def get_collect_interval(self):
        key = "collect_interval"
        if key in self.meta:
            return self.meta[key]
        return self.config[key]

    def is_collect_needed(self):
        timestamp = make_timestamp_epoch()
        if self.next_collect <= timestamp:
            self.next_collect = timestamp + self.get_collect_interval()
            return True
        return False

    def make_common_data(self, **ka):
        g = lambda k: self.config[k]
        return make_data(
            name=self.name,
            location=g("location"),
            room=g("room"),
            sensor_name=self.sensor_name,
            sensor_id=self.sensor_id,
            **ka
        )

    def setup(self):
        raise NotImplementedError()

    def collect(self):
        raise NotImplementedError()

    def write(self, data):
        data_output = self.config["data_output"]
        if data_output == "file":
            return self.write_file(data)
        elif data_output == "redis":
            return self.write_redis(data)
        raise Exception("UnknownDataOutput<{}>".format(data_output))

    def write_file(self, data):
        write(make_sensor_write_path(self), data)

    def write_redis(self, data):
        cli = get_redis_client(self.config)
        sensor_uid = make_sensor_uid(self)
        cli.set(sensor_uid, data)

    def __str__(self):
        def g(key):
            attr = getattr(self, key)
            if callable(attr):
                return attr()
            return attr

        return """[sensor / {get_collect_interval}]
        sensor_name: {sensor_name}
        sensor_id: {sensor_id}

        """.format(
            **{k: g(k) for k in ["sensor_name", "sensor_id", "get_collect_interval"]}
        )
