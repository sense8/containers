import http.server
import socketserver
import json


class HealthCheckHandler(http.server.BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header("Content-type", "application/json")
        self.end_headers()
        self.wfile.write(json.dumps({"banner": "ok"}).encode("utf8"))


async def healthcheck(host="127.0.0.1", port=8080, handler=HealthCheckHandler):

    with socketserver.TCPServer((host, port), handler) as httpd:
        print("serving at http://{host}:{port}".format(host=host, port=port))
        httpd.serve_forever()
