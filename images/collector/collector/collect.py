import json
import os
import time
import asyncio
import redis

from collector.logger import getLogger
from collector.utils import get_env
from collector.redis import get_redis_client
from collector.sensors.sensor_base import make_sensor_uid

logger = getLogger(__file__)


def write_sensor_list_file(config, sensors):
    pass


def write_sensor_list_redis(config, sensors):
    cli = get_redis_client(config)
    sensorSet = "Sensors"
    for sensor in sensors:
        cli.sadd(sensorSet, make_sensor_uid(sensor))


def write_sensor_list(config, sensors):
    output = config["data_output"]
    if output == "file":
        return write_sensor_list_file(config, sensors)
    elif output == "redis":
        return write_sensor_list_redis(config, sensors)
    raise Exception("UnknownDataOutput<{}>".format(output))


async def collect(config, sensors):
    write_sensor_list(config, sensors)
    while True:
        to_collect = []
        for sensor in sensors:
            if sensor.is_collect_needed():
                to_collect.append(sensor)
        if len(to_collect):
            await asyncio.gather(*[sensor.collect() for sensor in to_collect])
        time.sleep(5)
