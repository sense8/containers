import redis


redis_client = None


def get_redis_client(config):
    global redis_client
    if redis_client is None:
        redis_client = redis.StrictRedis(**config["redis"])
    return redis_client
