import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="sense8",  # Replace with your own username
    version="0.0.1",
    author="showi",
    author_email="showi@github.com",
    description="Sense8 tools",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/sense8/containers/lib/sense8",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.6",
)