import os
import shutil
from sense8.common import make_base_path

mandatory_files = {
    "etc": {
        "collector.json": True,
        "influxdb.env": True,
        "telegraf.conf": True,
    }
}


class FileNotFoundError(Exception):
    pass


make_etc_path = lambda path: make_base_path("etc", path)


def check_configuration():
    for fn in mandatory_files["etc"]:
        fp = make_etc_path(fn)
        if not os.path.exists(fp):
            raise FileNotFoundError(fp)
        print("[i] edit {}".format(fp))


def copy_samples():
    for fn in mandatory_files["etc"]:
        dest = make_etc_path(fn)
        src = "{}.sample".format(dest)
        if not os.path.exists(src):
            raise FileNotFoundError(src)
        if os.path.exists(dest):
            print("[i] skip copy {}".format(fn))
            continue
        shutil.copy(src, dest)
        print("[i] edit {}".format(dest))
