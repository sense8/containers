import os
import subprocess


def make_base_path(*paths):
    return os.path.join(
        os.path.abspath(
            os.path.join(
                os.path.dirname(__file__),
                os.path.pardir,
                os.path.pardir,
                os.path.pardir,
            )
        ),
        *paths
    )


def get_platform():
    result = subprocess.run(["docker", "info"], capture_output=True)
    if result.returncode != 0:
        raise Exception("getPlatformError")
    keys = ["OSType", "Architecture"]
    data = {}
    for line in result.stdout.split(b"\n"):
        try:
            key, value = [str(f.strip(), encoding="utf8") for f in line.split(b":")]
            if key in keys:
                data[key] = value
        except:
            pass
    if data["OSType"] == "linux":
        if data["Architecture"] == "x86_64":
            data["Architecture"] = "amd64"
        elif data["Architecture"] == "armv6l":
            data["Architecture"] = "arm/v6"
    return "/".join(data[k] for k in keys)


def read_env(env_path):
    with open(env_path, "r") as rh:
        for line in rh:
            yield [f.strip() for f in line.split("=")]
