#!/bin/sh

SENSE8=/sense8
SENSE8_ETC=${SENSE8}/etc
SENSE8_DOCKER_SUPERVISOR=showi/sense8-supervisor:0.0.1
SENSE8_BRANCH=master

if [[ ! -e ${SENSE8_ETC} ]]; then
    echo "[-] Missing etc directory ${SENSE8_ETC}"
fi

docker image rm -f ${SENSE8_DOCKER_SUPERVISOR}
docker run \
    --hostname sense8-node \
    --env SENSE8_BRANCH=${SENSE8_BRANCH} \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v /var/run/dbus/system_bus_socket:/var/run/dbus/system_bus_socket \
    -v ${SENSE8_ETC}:/sense8/etc \
    --device /dev/i2c-1 \
    -it ${SENSE8_DOCKER_SUPERVISOR} 

