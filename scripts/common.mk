PLATFORMS=linux/amd64,linux/arm64,linux/ppc64le,linux/386,linux/arm/v7,linux/arm/v6
PLATFORM_RPI=linux/arm/v6
# PLATFORMS=linux/arm/v6,linux/amd64
# unsupported: linux/riscv64,linux/s390x (cargo...)
DOCKER_NETWORK=sense8-net
DOCKER_REDIS_PORT=6379

.PHONY: build
build: build-march

.PHONY: check-env
check-env:
ifndef DOCKERHUB_USER 
	$(error DOCKERHUB_USER is undefined)
endif
ifndef TOPDIR 
	TOPDIR=.
endif

.PHONY: build-march
build-march: check-env setup-buildx
	$(info "BUILDX_OPTS: ${BUILDX_OPTS}")
	docker buildx build \
		$(BUILDX_OPTS) \
		--progress  plain \
		--compress \
		--push \
		--platform $(PLATFORMS) \
		--tag $(DOCKERHUB_USER)/$(TAG) .
	docker buildx build \
		$(BUILDX_OPTS) \
		--progress  plain \
		--compress \
		--push \
		--platform $(PLATFORM_RPI) \
		--tag $(DOCKERHUB_USER)/$(TAG)-rpi .

.PHONY: shell
shell:
	docker run -it $(DOCKERHUB_USER)/$(TAG) /bin/sh

.PHONY: create-network
create-network:
	$(TOPDIR)/scripts/docker-network-create $(DOCKER_NETWORK)

.PHONY: remove-network
remove-network:
	$(TOPDIR)/scripts/docker-network-remove $(DOCKER_NETWORK)

.PHONY: run
run: create-network
	docker run -it --network $(DOCKER_NETWORK) $(DOCKERHUB_USER)/$(TAG) $(CMD)

.PHONY: build-local
build-local: check-env
	docker build \
		--tag $(DOCKERHUB_USER)/$(TAG) \
		.

.PHONY: setup-buildx
setup-buildx:
# $(shell docker use sense8 && echo 1) && 
	# $(shell docker buildx create --name sense8 --platform ${PLATFORMS} 2>/dev/null)
	# docker buildx use sense8

.PHONY: clean
clean:
	./scripts/clean
	rm -rf env

env:
	python3 -m venv env
	./env/bin/pip install -r lib/sense8/requirements.txt

.PHONY: activate
activate:
	@echo source env/bin/activate

.PHONY: deactivate
deactivate:
	@echo deactivate