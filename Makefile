include ./scripts/common.mk

CONTAINERS=base-image redis collector ble display sowatch supervisor telegraf

###

IMAGES=$(foreach container,$(CONTAINERS),"images/$(container)")
.PHONY: build
build:
	$(MAKE) build-march

.PHONY: build-march
build-march: $(IMAGES)

$(IMAGES):
	$(info IMAGES: $(IMAGES))
	cd $@; $(MAKE) build-march
